CREATE FUNCTION numero_menor_mil ( IN
 :numero double, :bloque INT )
 RETURNS VARCHAR(50);
 -- bloque = 1 indica que es la seccion de los cientos
 --  del numero completo
 BEGIN
  DECLARE :unidades 			INT;
  DECLARE :decenas				INT;
  DECLARE :centenas				INT;
  
  -- guarda por separado los resultados
  DECLARE :unidades_t 			VARCHAR(15);
  DECLARE :decenas_t			VARCHAR(15);
  DECLARE :centenas_t			VARCHAR(15);
  DECLARE :unir					VARCHAR(3);
  DECLARE :espacio				VARCHAR(1);
  
  IF (:numero = 100) THEN
    RETURN (SELECT RTRIM(descripcion) FROM ap_func_letras WHERE bloque = 3 AND valor = :numero );   
  END IF
  
  SET :centenas = truncate(:numero / 100,0);
  SET :decenas = truncate(mod(:numero ,100)/10,0);
  SET :unidades = mod(:numero ,10);
  --
  -- CENTENAS
  IF (:centenas > 0) THEN
    SET :centenas_t = (SELECT RTRIM(descripcion) FROM ap_func_letras WHERE bloque = 3 AND valor = :centenas );
  END IF;
  -- DECENAS
  IF (:decenas > 1) THEN
    SET :decenas_t = (SELECT RTRIM(descripcion) FROM ap_func_letras WHERE bloque = 2 AND valor = :decenas );
    SET :unidades_t = (SELECT RTRIM(descripcion) FROM ap_func_letras WHERE bloque = 1 AND valor = :unidades );
    IF (:unidades > 0) THEN
      SET :unir	= ' Y ';
    END IF;
    SET :espacio = ' ';
  END IF;

  IF (:decenas = 1) THEN
      SET :unidades_t = (SELECT RTRIM(descripcion) FROM ap_func_letras WHERE bloque = 2 AND valor = :decenas * 10 + :unidades );
      SET :espacio = ' ';
  END IF;
  
  IF (:decenas = 0) THEN
    SET :unidades_t = (SELECT RTRIM(descripcion) FROM ap_func_letras WHERE bloque = 1 AND valor = :unidades );
    SET :espacio = ' ';
  END IF;
  
  IF (:unidades = 1) THEN
    IF (:decenas <> 1) THEN
      IF (:bloque > 1) THEN
        SET :unidades_t = 'UN'; -- Caso miles, millones ...
      END IF;
    END IF;
  END IF;
  
  RETURN RTRIM( ISNULL(:centenas_t,'')) + ISNULL(:espacio,'') + RTRIM( ISNULL(:decenas_t,'')) + ISNULL(:unir,'') + RTRIM( ISNULL(:unidades_t,''));
 END;
