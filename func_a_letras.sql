CREATE FUNCTION func_a_letras ( IN
 :numero double )
 RETURNS VARCHAR(200);
 BEGIN
  DECLARE :mil_millon 			INT;
  DECLARE :millon				INT;
  DECLARE :miles				INT;
  DECLARE :centenas				INT;
  DECLARE :centimos				INT;
  DECLARE :entero				INT;
  DECLARE :en_letras 			VARCHAR(200);
  DECLARE :aux					VARCHAR(15);
--
-- Variables para guardar cada porcion en texto
  DECLARE :centimos_t			VARCHAR(10);
  DECLARE :centimos_at			VARCHAR(5);
  DECLARE :centimos_dt			VARCHAR(5);
  DECLARE :centenas_t		    VARCHAR(50);
  DECLARE :miles_t				VARCHAR(50);
  DECLARE :millon_t				VARCHAR(50);
  DECLARE :mil_millon_t			VARCHAR(50);
  DECLARE :mil_millon_t_post	VARCHAR(15);
  DECLARE :mil_millon_t_esp		VARCHAR(1);
  DECLARE :millon_t_post		VARCHAR(15);
  DECLARE :millon_t_esp			VARCHAR(1);
  DECLARE :miles_t_post			VARCHAR(15);
  DECLARE :miles_t_esp			VARCHAR(1);
--
-- separar el numero en hasta cuatro partes de 3 en 3  
  SET :entero = TRUNCATE(:numero,0);
  SET :mil_millon = MOD(round(:numero,-3)/1000,1000000000) / 1000000;
  SET :millon = MOD(:numero, 1000000000) / 1000000;
  SET :miles = MOD(:numero, 1000000)/1000;
  SET :centenas = MOD(:numero, 1000);
  SET :centimos = MOD(round(:numero,2)*100,100);
  
  IF (:centimos > 0) THEN 
    IF ABS(:centimos - ( :numero - :entero )*100)  > 0.0001 THEN
      SET :centimos = :centimos + 1;
    END IF;
  END IF;
  
  IF (:centimos = 0) THEN
    SET :centimos_t = ' EXACTOS';
  ELSE
    SET :centimos_at = ' CON ';
    SET :centimos_dt = '/100';
    SET :centimos_t = :centimos;
  END IF;
--
-- pasar a texto miles de millon
  IF (:mil_millon >= 1) THEN
    -- en la funcion numero_menor_mil 4 (parametro 2) indica la posicion
    --  de la porcion en el num completo, que esta dividido en 
    --  segmentos de 3. 4ta porcion -> [999],999,999,999.99
    SET :mil_millon_t = LTRIM(RTRIM(numero_menor_mil(:mil_millon, 4)));
    SET :mil_millon_t_post = 'MIL ';
    SET :mil_millon_t_esp = ' ';
    IF :millon = 0 THEN
      SET :millon_t_post = 'MILLONES ';
      SET :millon_t_esp = ' ';
    END IF;
  END IF;
--
-- pasar a texto millon
  IF (:millon >= 1) THEN
    SET :millon_t = LTRIM(RTRIM(numero_menor_mil(:millon, 3)));
    SET :millon_t_esp = ' ';
    IF :millon = 1 THEN
      SET :millon_t_post = 'MILLON ';
    ELSE
      SET :millon_t_post = 'MILLONES ';
    END IF;
  END IF;
--
-- pasar a texto miles
  IF (:miles >= 1) THEN
    SET :miles_t = LTRIM(RTRIM(numero_menor_mil(:miles, 2)));
    SET :miles_t_esp = ' ';
    SET :miles_t_post = 'MIL ';
  END IF;
--
-- Pasar a texto las centenas
  IF (:centenas >= 1) THEN
    SET :centenas_t = LTRIM(RTRIM(numero_menor_mil(:centenas,1)));
  END IF
--
-- construir monto en letras
  SET :en_letras = RTRIM(ISNULL(:mil_millon_t,''))+ISNULL(:mil_millon_t_esp,'')+ISNULL(:mil_millon_t_post,'')+RTRIM(ISNULL(:millon_t,''))+ISNULL(:millon_t_esp,'')+ISNULL(:millon_t_post,'')+RTRIM(ISNULL(:miles_t,''))+ISNULL(:miles_t_esp,'')+ISNULL(:miles_t_post,'')+RTRIM(ISNULL(:centenas_t,''));
--
-- devolver valor  
  RETURN RTRIM(ISNULL(:en_letras,''))+ISNULL(:centimos_at,'')+:centimos_t+ISNULL(:centimos_dt,'');
--
END;
